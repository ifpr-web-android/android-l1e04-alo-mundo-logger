package br.edu.ifpr.android.l1e4alomundolog;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class AloMundoLogActivity extends AppCompatActivity {


    public static final String TAG = "AloMundoLogActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alo_mundo_log);


        try{
            throw new Exception(getString(R.string.erro_fatal));
        }
        catch(Exception e){
            Log.e(TAG, getString(R.string.alo_mundo), e);
        }


    }
}
